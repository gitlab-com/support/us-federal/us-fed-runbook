# US Federal On-call Run Books

Inspired by the on-call runbook for [Infrastructure Reliability Engineers](https://gitlab.com/gitlab-com/runbooks), this project provides a guidance for US Federal Support Engineers and Managers who are starting an on-call shift or responding to an incident. If you haven't yet, review the [On-call in GitLab Support](https://about.gitlab.com/handbook/support/on-call/) page in the handbook before reading on.

## On-Call

GitLab US Federal Support Engineers and Managers provide on-call coverage 7 days a week between the hours of 0500 and 1700 Pacific Time to ensure incidents are responded to promptly and resolved as quickly as possible.

## Special Considerations

Our US Federal customers have unique requirements regarding sensitive data and may not be able to share their screen, logs, or configuration files without sanitizing key items in each. Before requesting information from a customer, ensure they're comfortable with sharing said data, or redacting sensitive information before sharing. Examples of sensitive data include:

  * Usernames
  * Passwords/tokens
  * IP Addresses
  * Project paths/project names
